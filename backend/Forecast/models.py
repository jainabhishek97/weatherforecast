from django.db import models

class weatherdata(models.Model):

	city = models.CharField(max_length=100);
	date = models.CharField(max_length=100);
	temprature = models.CharField(max_length=100);
	pressure = models.CharField(max_length=100);
	humidity = models.CharField(max_length=100);
	weather = models.CharField(max_length=100);
