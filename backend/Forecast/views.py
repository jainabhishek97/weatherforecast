from django.shortcuts import render
from django.http import HttpResponse
import json
import requests
from django.views.generic import View
from .models import weatherdata
import json


class MyView(View):

    def get(self, request, city, mydate):

        response = {'date':"", 'temprature' : "", 'humidity':"",'pressure': "", 'forecast':""}

                # print(mydate)
        if(weatherdata.objects.filter(city=city,date = str(mydate))):
            current_row = weatherdata.objects.filter(city=city,date = str(mydate)).first();
            response['date'] = current_row.date;
            response['temprature'] = current_row.temprature;
            response['humidity'] = current_row.humidity;
            response['pressure'] = current_row.pressure;
            response['forecast'] = current_row.weather;
            # dump = json.dumps(response)
            # return HttpResponse(dump, content_type='application/json')

        else:    
            
            url = "https://openweathermap.org/data/2.5/forecast?q=" + str(city) + "&appid=b6907d289e10d714a6e88b30761fae22"
            Response_Data = requests.get(url).json()
            j=0
            for i in Response_Data['list']:
                

                response = {
                    'date': Response_Data['list'][j]['dt_txt'],
                    'temprature': Response_Data['list'][j]['main']['temp'],
                    'humidity': Response_Data['list'][j]['main']['humidity'],
                    'pressure': Response_Data['list'][j]['main']['pressure'],
                    'forecast' : Response_Data['list'][j]['weather'][0]['description']
                }
                j=j+1
                newdate = response['date'][0:10]
                # print(mydate)
                # print(newdate)
                if(newdate == mydate):
                    break  
                # print(response)

            current_city= weatherdata()
            current_city.city = city
            current_city.date = mydate
            current_city.temprature = response['temprature']
            current_city.pressure = response['pressure']
            current_city.humidity = response['humidity']
            current_city.weather = response['forecast']
            current_city.save()

        dump = json.dumps(response)
        return HttpResponse(dump, content_type='application/json')



