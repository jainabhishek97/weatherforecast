from django.conf.urls import include, url
from Forecast import views as myviews
from Forecast.views import MyView

urlpatterns = (
   url(r'^forecast/(?P<city>[\w\-]+)/(?P<mydate>[\w\-]+)/$',MyView.as_view()),
)