export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    })
    .state('test',{
    	url: '/test',
    	controller: 'TestController',
    	templateUrl: 'app/test/test.html',
    	controllerAs: 'testCtrl'
    })
    .state('test1',{
      url: '/test1',
      controller: 'TestController1',
      templateUrl: 'app/test/test1.html',
      controllerAs: 'testCtrl1'
    });

  $urlRouterProvider.otherwise('/');
}
